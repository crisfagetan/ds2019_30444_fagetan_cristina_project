package ds.project.assignment4_doctor_consumer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@NoArgsConstructor
@Data
public class PatientActivity {

    int id;
    int patientId;

    Date startTime;

    Date endTime;

    String activity;

    String status;

    String recommend;

   public  PatientActivity(int id, int patientId, Date startTime, Date endTime, String activity, String recommend, String status){
       this.id = id;
       this.patientId = patientId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
       this.status = status;
        this.recommend = recommend;

    }



    public void statusSet(String activity){
        int hours = (int) (((Math.abs(this.getEndTime().getTime() - this.getStartTime().getTime())) / (1000 * 60 * 60)));
        int minutes  = (int) (((Math.abs(this.getEndTime().getTime() - this.getStartTime().getTime())) / (1000 * 60)) % 60);
        int seconds = (int) (((Math.abs(this.getEndTime().getTime() - this.getStartTime().getTime())) / 1000) % 60);

        switch (activity){
            // R1: The sleep period longer than 12 hours;
            case "Sleeping":
                if(hours > 12 || (hours == 12 && (minutes > 0 || seconds > 0))){
                    this.status = "Too much Sleep";
                }
                break;
            // R2: The leaving activity (outdoor) is longer than 12 hours;
            case "Leaving":
                if(hours > 12 || (hours == 12 && (minutes > 0 || seconds > 0))){
                    this.status = "To much stayed outside";
                }
                break;
            // R3: The period spent in bathroom is longer than 1 hour;
            case "Toileting":
                if(hours > 1 || (hours == 1 && (minutes > 0 || seconds > 0))){
                    this.status = "To much time in the toilet";
                }
                break;
            case "Showering":
                if(hours > 1 || (hours == 1 && (minutes > 0 || seconds > 0))){
                    this.status = "To much time in the toilet";
                }
                break;
            case "Grooming":
                if(hours > 1 || (hours == 1 && (minutes > 0 || seconds > 0))){
                    this.status = "To much time in the toilet";
                }
                break;
            default: this.status = "OK";
        }
    }

}
