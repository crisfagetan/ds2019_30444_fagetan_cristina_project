package ds.project.assignment4_doctor_consumer.ui;

import com.example.consumingwebservice.wsdl.MonitoredData;
import ds.project.assignment4_doctor_consumer.DoctorClient;
import ds.project.assignment4_doctor_consumer.PatientActivity;
import ds.project.assignment4_doctor_consumer.dtos.MonitoredDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private DoctorClient soapService;

    @Autowired
    DoctorController(DoctorClient doctorClient){
        soapService = doctorClient;
    }


    @GetMapping(value = "/activities/{patientId}")
    public List<PatientActivity> getActivities(@PathVariable("patientId") Integer patientId)
    {
        return soapService.getActivities(patientId);
    }

    @GetMapping(value = "/activities/{patientId}/{id}")
    public Optional<PatientActivity> getActivities(@PathVariable("id") Integer id, @PathVariable("patientId") Integer patientId)
    {
        return soapService.getActivities(patientId).stream().filter(a -> a.getId() == id).findFirst();
    }

    @PutMapping(value = "/activities")
    public int updateUser(@RequestBody MonitoredDataDTO monitoredData) { return soapService.changeActivities(monitoredData); }

}
