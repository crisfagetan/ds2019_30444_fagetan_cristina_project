package ds.project.assignment4_doctor_consumer;

import com.example.consumingwebservice.wsdl.*;
import ds.project.assignment4_doctor_consumer.dtos.MonitoredDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DoctorClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(DoctorClient.class);


    public GetLoginResponse getLogin(String username, String password) {

        GetLoginRequest request = new GetLoginRequest();
        request.setUsername(username);
        request.setPassword(password);

        log.info("Requesting permission for " + username);

        GetLoginResponse response = (GetLoginResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8082/ws/users/login", request,
                        new SoapActionCallback(
                                "http://doctorApp/doctor/GetLoginRequest"));

        return response;
    }

    public List<PatientActivity> getActivities(int patientId) {

        GetActivityRequest request = new GetActivityRequest();
        request.setPatientId(patientId);

        log.info("Requesting activity for patient " + patientId);

        GetActivityResponse response = (GetActivityResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8082/ws/activities", request,
                        new SoapActionCallback(
                                "http://doctorApp/doctor/GetActivityRequest"));

        List<PatientActivity> activities = new ArrayList<>();
        for (MonitoredData m:response.getActivity()) {
            PatientActivity pa = new PatientActivity(m.getId(), m.getPatientId(), toDate(m.getStartTime()), toDate(m.getEndTime()), m.getActivity(), m.getRecommend(), m.getStatus());
          // if(pa.getRecommend().equals(null))
                pa.statusSet(m.getActivity());
            activities.add(pa);
        }

        return activities;
    }

    public int changeActivities(MonitoredDataDTO monitoredDataDTO) {

        ChangeActivityRequest request = new ChangeActivityRequest();

        MonitoredData monitoredData =  new MonitoredData();
        PatientActivity patientActivity = MonitoredDataDTO.generateEntityFromDTO(monitoredDataDTO);
        monitoredData.setActivity(patientActivity.getActivity());
        monitoredData.setEndTime(dateToString(patientActivity.getEndTime()));
        monitoredData.setStartTime(dateToString(patientActivity.getStartTime()));
        monitoredData.setPatientId(patientActivity.getPatientId());
        monitoredData.setRecommend(patientActivity.getRecommend());
        monitoredData.setPatientId(patientActivity.getId());
        monitoredData.setStatus(patientActivity.getStatus());
        monitoredData.setId(patientActivity.getId());

        request.setActivity(monitoredData);

        log.info("Requesting activity for patient " + monitoredDataDTO.getActivity());

        ChangeActivityResponse response = (ChangeActivityResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8082/ws/activities", request,
                        new SoapActionCallback(
                                "http://doctorApp/doctor/ChangeActivityRequest"));

        return response.getChanged();
    }

    private static Date toDate(String d)  {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date data = null;
        try {
            data = format.parse(d);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return data;
    }
    public String dateToString(Date d) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return format.format(d);
    }
}

