package ds.project.assignment4_doctor_consumer.dtos;

import com.example.consumingwebservice.wsdl.MonitoredData;
import ds.project.assignment4_doctor_consumer.PatientActivity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoredDataDTO {

    int id;

    int patientId;

    Date startTime;

    Date endTime;

    String activity;

    String recommend;

    String status;

    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static MonitoredDataDTO generateDTOFromEntity(MonitoredData monitoredDataEntity){
        return new MonitoredDataDTO(
                monitoredDataEntity.getId(),
                monitoredDataEntity.getPatientId(),
                toDate(monitoredDataEntity.getStartTime()),
                toDate(monitoredDataEntity.getEndTime()),
                monitoredDataEntity.getActivity(),
                monitoredDataEntity.getRecommend(),
                monitoredDataEntity.getStatus());
    }

    public static PatientActivity generateEntityFromDTO(MonitoredDataDTO forViewDTO){
        return new PatientActivity(
                forViewDTO.id,
                forViewDTO.patientId,
                forViewDTO.startTime,
                forViewDTO.endTime,
                forViewDTO.activity,
                forViewDTO.recommend,
                forViewDTO.status
        );
    }

    private static Date toDate(String d)  {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date data = null;
        try {
            data = format.parse(d);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return data;
    }
}

