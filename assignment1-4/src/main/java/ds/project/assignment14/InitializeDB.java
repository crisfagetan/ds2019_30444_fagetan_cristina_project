package ds.project.assignment14;

import ds.project.assignment14.dtos.DrugDTO;
import ds.project.assignment14.dtos.UserForRegisterDTO;
import ds.project.assignment14.entities.User;
import ds.project.assignment14.services.DrugService;
import ds.project.assignment14.services.PatientService;
import ds.project.assignment14.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class InitializeDB implements CommandLineRunner {

    UserService userService;
    DrugService drugService;
    PatientService patientService;

    @Autowired
    InitializeDB(UserService userService, DrugService drugService, PatientService patientService){
        this.userService = userService;
        this.drugService = drugService;
        this.patientService = patientService;
    }


    @Override
    public void run(String... args) throws Exception {
        UserForRegisterDTO admin = new UserForRegisterDTO(1, "admin", "password", 0, "Ion", "Ion", new Date(1989, 2, 12), true, "");
        UserForRegisterDTO caregiver = new UserForRegisterDTO(2, "caregiver", "password", 1, "Maria", "Ionescu", new Date(1970, 5, 19), false, "");
        UserForRegisterDTO patient = new UserForRegisterDTO(3, "patient", "password", 2, "Andrei", "Vasile", new Date(1995, 10, 31), true, "");
        UserForRegisterDTO patient1 = new UserForRegisterDTO(4, "patient1", "password", 2, "Tudor", "Blaga", new Date(1984, 4, 6), true, "");
        UserForRegisterDTO patient2 = new UserForRegisterDTO(5, "patient2", "password", 2, "Georgiana", "Tatu", new Date(2000, 9, 7), false, "");

        userService.insert(admin);
        userService.insert(caregiver);
        userService.insert(patient);
        userService.insert(patient1);
        userService.insert(patient2);

//        patientService.addCaregiver(1, 1);
//        patientService.addCaregiver(2, 1);
//        patientService.addCaregiver(3, 1);


        DrugDTO drug1 = new DrugDTO(1, "Algocalmin", "Somnolenta", 3);
        DrugDTO drug2 = new DrugDTO(2, "Paracetamol", "", 2);
        DrugDTO drug3 = new DrugDTO(3, "Bilomag", "Dependenta", 1);

        drugService.insert(drug1);
        drugService.insert(drug2);
        drugService.insert(drug3);
    }
}
