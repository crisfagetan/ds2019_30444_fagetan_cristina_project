package ds.project.assignment14.server;

import io.grpc.ServerBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GrpcServer implements CommandLineRunner {

    private final HelloServiceImpl service;

    @Override
    public void run(String... args) throws Exception {
        try {
            ServerBuilder.forPort(6565).addService(service).build().start().awaitTermination();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
