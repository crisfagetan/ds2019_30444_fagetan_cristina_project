package ds.project.assignment14;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import ds.project.assignment14.entities.MonitoredData;
import ds.project.assignment14.services.MonitoredDataService;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Controller
@CrossOrigin
public class Receiver {


    private final static String QUEUE_NAME = "hello";
    private int i;

    private ConnectionFactory factory = null;
    private JSONParser parser;

    private MonitoredDataService monitoredDataService;
    private final SimpMessagingTemplate template;

    @Autowired
    public Receiver(MonitoredDataService monitoredDataService, SimpMessagingTemplate template)
    {
        this.monitoredDataService = monitoredDataService;
        this.template = template;
        parser = new JSONParser();
        this.i = 1;
    }

    @RabbitListener(queues = QUEUE_NAME)
    public void run ()
    {
        try {
            factory = new ConnectionFactory();
            factory.setHost("ds-rabbitmq");
//        factory.setPort(5676);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");


            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                try {
                    String message = new String(delivery.getBody());
                    JSONObject obj = (JSONObject) parser.parse(message);
                    MonitoredData monitoredData = new MonitoredData(Integer.parseInt(obj.get("patient_id").toString()),
                                                                    obj.get("activity").toString(),
                                                                    toDate(obj.get("start").toString()),
                                                                    toDate(obj.get("end").toString()));
//                    System.out.println(" [xxx] Received '" + monitoredData + "'");
                    monitoredDataService.insert(monitoredData);
                    String msg = "";
                    String alert = "";
                    int check = check(monitoredData);
                    switch (check){
                        case 1:
                            msg = i + "** Too much SLEEP";
                            alert = "Patient " + monitoredData.getPatientId() + " had too much SLEEP!";
                            break;
                        case 2:
                            msg =  i + "** Too much LEAVING";
                            alert = "Patient " + monitoredData.getPatientId() + " was LEAVING for too long!";
                            break;
                        case 3:
                            msg = i + "** Too much TOILET";
                            alert = "Patient " + monitoredData.getPatientId() + " had stayed too much in the TOILET!";
                            break;
                        default:
                            msg = i + "** Status OK";
                            break;
                    }
                    System.out.println(msg);
                    if(check != 0){
                        send(alert);
                    }


                    i++;

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };
            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private static Date toDate(String d)  {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date data = null;
        try {
            data = format.parse(d);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return data;
    }


    private int check(MonitoredData x){
       int hours = (int) (((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / (1000 * 60 * 60)));
       int minutes  = (int) (((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / (1000 * 60)) % 60);
       int seconds = (int) (((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / 1000) % 60);

       switch (x.getActivity()){
           // R1: The sleep period longer than 12 hours;
           case "Sleeping":
               if(hours > 12 || (hours == 12 && (minutes > 0 || seconds > 0))){
                   return 1;
               }
               break;
               // R2: The leaving activity (outdoor) is longer than 12 hours;
           case "Leaving":
               if(hours > 12 || (hours == 12 && (minutes > 0 || seconds > 0))){
                   return 2;
               }
               break;
               // R3: The period spent in bathroom is longer than 1 hour;
           case "Toileting":
               if(hours > 1 || (hours == 1 && (minutes > 0 || seconds > 0))){
                   return 3;
               }
               break;
           case "Showering":
               if(hours > 1 || (hours == 1 && (minutes > 0 || seconds > 0))){
                   return 3;
               }
               break;
           case "Grooming":
               if(hours > 1 || (hours == 1 && (minutes > 0 || seconds > 0))){
                   return 3;
               }
               break;
           default: return 0;
       }
       return 0;
    }


    public void send(String message){
        this.template.convertAndSend("/topic/event",  message);
    }



}
