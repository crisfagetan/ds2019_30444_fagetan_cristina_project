package ds.project.assignment14.services;


import ds.project.assignment14.dtos.MedPlanViewDTO;
import ds.project.assignment14.dtos.PatientViewDTO;
import ds.project.assignment14.entities.Caregiver;
import ds.project.assignment14.entities.Patient;
import ds.project.assignment14.errorhandlers.ResourceNotFoundException;
import ds.project.assignment14.repositories.CaregiverRepository;
import ds.project.assignment14.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
    }

    //------------------------------------------ FIND -------------------------------------------------------------------

    public List<PatientViewDTO> findAll(){
        List<Patient> patients = patientRepository.findAllByIsRemoved(false);

        return patients.stream()
                .map(PatientViewDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public PatientViewDTO findPatientById(Integer id){
        Patient patient = patientRepository.findPatientByIdAndIsRemoved(id, false);

        if (patient == null) {
            throw new ResourceNotFoundException("Patient", "patient id", id);
        }

        return PatientViewDTO.generateDTOFromEntity(patient);
    }

    //TODO see medPlans
    public List<MedPlanViewDTO> getMedplans(Integer patientId){
        Patient patient = patientRepository.findPatientByIdAndIsRemoved(patientId, false);

        if (patient == null) {
            throw new ResourceNotFoundException("Patient", "patient id", patientId);
        }

        return patient.getMedPlans().stream()
                .map(MedPlanViewDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    //------------------------------------------------ CREATE ----------------------------------------------------------
    public Integer insertPatient(Patient patient){
        try{
            return patientRepository
                    .save(patient)
                    .getId();
        }
        catch (Exception e){
            throw e;
        }

    }

    //------------------------------------------------- UPDATE ---------------------------------------------------------


    public Integer update(PatientViewDTO patientViewDTO) {

        Patient patient = patientRepository.findPatientByIdAndIsRemoved(patientViewDTO.getId(), false);

        if(patient == null){
            throw new ResourceNotFoundException("Patient", "Patient id", patientViewDTO.getId().toString());
        }
        //TODO
       // UserFieldValidator.validateRegister(patientViewDTO);

        Patient patientToUpdate = PatientViewDTO.generateEntityFromDTO(patientViewDTO, patient.getPatientUser());


        return patientRepository.save(patientToUpdate).getId();
    }

    //TODO add caregiver
    public void addCaregiver(Integer patientId, Integer caregiverId){
        Patient patient = patientRepository.findPatientByIdAndIsRemoved(patientId, false);

        if(patient == null){
            throw new ResourceNotFoundException("Patient", "Patient id", patientId.toString());
        }

        Caregiver caregiver = caregiverRepository.findCaregiverByIdAndIsRemoved(caregiverId, false);

        if(caregiver == null){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverId.toString());
        }

//        Patient patientToUpdate = PatientViewDTO.generateEntityFromDTO(patientViewDTO, patient.getPatientUser());
//        Caregiver caregiverToUpdate = CaregiverViewDTO.generateEntityFromDTO(caregiverViewDTO, caregiver.getCaregiverUser());
        List<Patient> patients = caregiver.getPatients();
        patients.add(patient);
        caregiver.setPatients(patients);

        List<Caregiver> caregivers = patient.getCaregivers();
        caregivers.add(caregiver);
        patient.setCaregivers(caregivers);

        patientRepository.save(patient);
        caregiverRepository.save(caregiver);

    }

    //------------------------------------------------ DELETE ----------------------------------------------------------

    public void delete(PatientViewDTO patientViewDTO)
    {
        Patient patient = patientRepository.findPatientByIdAndIsRemoved(patientViewDTO.getId(), false);
        patient.setIsRemoved(true);
        patient.getPatientUser().setIsRemoved(true);
        patient.getMedPlans().forEach(medicationPlan -> medicationPlan.setIsRemoved(true));
        patient.getCaregivers().clear();
        //patient.getCaregivers().forEach(caregiver -> caregiver.getPatients().clear());
        patientRepository.save(patient);
    }




}
