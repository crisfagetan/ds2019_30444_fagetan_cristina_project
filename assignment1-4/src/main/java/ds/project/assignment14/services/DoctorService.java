package ds.project.assignment14.services;


import ds.project.assignment14.dtos.DoctorViewDTO;
import ds.project.assignment14.entities.Doctor;
import ds.project.assignment14.errorhandlers.ResourceNotFoundException;
import ds.project.assignment14.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    //------------------------------------------ FIND -------------------------------------------------------------------

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctors = doctorRepository.findAllByIsRemoved(false);

        return doctors.stream()
                .map(DoctorViewDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public DoctorViewDTO findUserById(Integer id){
        Doctor doctor = doctorRepository.findDoctorByIdAndIsRemoved(id, false);

        if (doctor == null) {
            throw new ResourceNotFoundException("Doctor", "doctor id", id);
        }

        return DoctorViewDTO.generateDTOFromEntity(doctor);
    }

    //------------------------------------------------ CREATE ----------------------------------------------------------
    public Integer insertDoctor(Doctor doctor){
        try{
            return doctorRepository
                    .save(doctor)
                    .getId();
        }
        catch (Exception e){
            throw e;
        }
    }

    //------------------------------------------------- UPDATE ---------------------------------------------------------


    public Integer update(DoctorViewDTO doctorViewDTO) {

        Doctor doctor = doctorRepository.findDoctorByIdAndIsRemoved(doctorViewDTO.getId(), false);

        if(doctor == null){
            throw new ResourceNotFoundException("Doctor", "doctor id", doctorViewDTO.getId().toString());
        }
        //TODO
       // UserFieldValidator.validateRegister(doctorViewDTO);

        Doctor doctorToUpdate = DoctorViewDTO.generateEntityFromDTO(doctorViewDTO, doctor.getDoctorUser());

        return doctorRepository.save(doctorToUpdate).getId();
    }


    //------------------------------------------------ DELETE ----------------------------------------------------------

   //TODO ce se intampla cu medplan cand sterg doctorul???
    public void delete(DoctorViewDTO doctorViewDTO){
        Doctor doctor = doctorRepository.findDoctorByIdAndIsRemoved(doctorViewDTO.getId(), false);
        doctor.setIsRemoved(true);
        doctor.getDoctorUser().setIsRemoved(true);
        doctorRepository.save(doctor);
    }




}
