package ds.project.assignment14.repositories;

import ds.project.assignment14.entities.MonitoredData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitoredDataRepository extends JpaRepository<MonitoredData, Integer> {
}
