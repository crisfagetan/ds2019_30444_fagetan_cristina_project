package ds.project.assignment14.repositories;

import ds.project.assignment14.entities.Patient;
import ds.project.assignment14.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

     public Patient getPatientByPatientUser(User userId);
     public Patient findPatientByIdAndIsRemoved(int id, Boolean isRemoved);
     public Patient findPatientByPatientUser(User user);
     public List<Patient> findAllByIsRemoved(Boolean isRemoved);


}

