package ds.project.assignment14.repositories;

import ds.project.assignment14.entities.Doctor;
import ds.project.assignment14.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {


     public Doctor findDoctorByDoctorUser(User userId);
     public Doctor findDoctorByIdAndIsRemoved(int id, Boolean isRemoved);
     public List<Doctor> findAllByIsRemoved(Boolean isRemoved);

}

