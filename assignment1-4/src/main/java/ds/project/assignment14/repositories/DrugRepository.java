package ds.project.assignment14.repositories;

import ds.project.assignment14.entities.Drug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DrugRepository extends JpaRepository<Drug, Integer> {

    @Query(value = "SELECT u " +
            "FROM Drug u " +
            "WHERE u.isRemoved = false " +
            "ORDER BY u.name")
    List<Drug> getAllOrdered();

    public Drug findDrugByIdAndIsRemoved(int id, Boolean isRemoved);
}
