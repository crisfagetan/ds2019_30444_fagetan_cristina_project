package ds.project.assignment14.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    private final SimpMessagingTemplate template;

    @Autowired
    public WebSocketController(SimpMessagingTemplate template){
        this.template = template;
    }

   // @MessageMapping("/send/message")
//    @SendTo("/topic/event")
    public void send(String message){
        this.template.convertAndSend("/topic/event",  message);
    }



}
