package ds.project.assignment14.controllers;

import ds.project.assignment14.dtos.DoctorViewDTO;
import ds.project.assignment14.dtos.MedPlanToAddDTO;
import ds.project.assignment14.services.DoctorService;
import ds.project.assignment14.services.MedPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;
    private final MedPlanService medPlanService;

    @Autowired
    public DoctorController(DoctorService doctorService, MedPlanService medPlanService) {
        this.doctorService = doctorService;
        this.medPlanService = medPlanService;
    }

    @GetMapping(value = "/{id}")
    public DoctorViewDTO findById(@PathVariable("id") Integer id){
        return doctorService.findUserById(id);
    }


    @GetMapping()
    public List<DoctorViewDTO> findAll(){
        return doctorService.findAll();
    }

    //Doctor Adds a medication Plan for patient
    @PostMapping(value = "/{doctorId}/{patientId}/medPlan")
    public Integer insertMedPlan(@RequestBody MedPlanToAddDTO medPlanToAddDTO, @PathVariable("doctorId") Integer doctorId, @PathVariable("patientId") Integer patientId){
        return medPlanService.insert(medPlanToAddDTO, doctorId, patientId);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody DoctorViewDTO doctorViewDTO) {
        return doctorService.update(doctorViewDTO);
    }

    @PutMapping(value = "/delete")
    public void delete(@RequestBody DoctorViewDTO doctorViewDTO){
        doctorService.delete(doctorViewDTO);
    }

}
