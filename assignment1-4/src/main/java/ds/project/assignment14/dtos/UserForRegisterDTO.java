package ds.project.assignment14.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.project.assignment14.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserForRegisterDTO {


    private Integer id;

    private String username;

    private String password;

    private Integer role;  // 0 = doctor; 1 = caregiver; 2 = patient

    private String firstName;
    private String lastName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Date dateOfBirth;

    private Boolean gender; //1 = male; 0 = female

    private String address;


    //------------------------------------------- GET & SET ------------------------------------------------------------

    public Boolean isDoctor() {
        return (role == 0);
    }

    public Boolean isCaregiver() {
        return (role == 1);
    }

    public Boolean isPatient() {
        return (role == 2);
    }


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static UserForRegisterDTO generateDTOFromEntity(User user){
        return new UserForRegisterDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getRole(),
                user.getFirstName(),
                user.getLastName(),
                user.getDateOfBirth(),
                user.getGender(),
                user.getAddress());
    }

    public static User generateEntityFromDTO(UserForRegisterDTO userForRegisterDTO){
        return new User(
                userForRegisterDTO.getId(),
                userForRegisterDTO.getUsername(),
                userForRegisterDTO.getPassword(),
                userForRegisterDTO.getRole(),
                userForRegisterDTO.getFirstName(),
                userForRegisterDTO.getLastName(),
                userForRegisterDTO.getDateOfBirth(),
                userForRegisterDTO.getGender(),
                userForRegisterDTO.getAddress(),
                false);
    }
}
