package ds.project.assignment14.dtos;

import ds.project.assignment14.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserForViewDTO {

    private Integer id;

    private String username;

    private Integer role;  // 0 = doctor; 1 = caregiver; 2 = patient

    private Integer userInRoleId;

    public Boolean isDoctor() {
        return (role == 0);
    }

    public Boolean isCaregiver() {
        return (role == 1);
    }

    public Boolean isPatient() {
        return (role == 2);
    }


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static UserForViewDTO generateDTOFromEntity(User user){
        return new UserForViewDTO(
                user.getId(),
                user.getUsername(),
                user.getRole(),
                0);
    }

    public static UserForViewDTO generateDTOFromEntityExtended(User user, Integer userInRoleId){
        return new UserForViewDTO(
                user.getId(),
                user.getUsername(),
                user.getRole(),
                userInRoleId);
    }

    public static User generateEntityFromDTO(UserForViewDTO forViewDTO){
        return new User(
                forViewDTO.getId(),
                forViewDTO.getUsername(),
                forViewDTO.getRole());
    }



}
