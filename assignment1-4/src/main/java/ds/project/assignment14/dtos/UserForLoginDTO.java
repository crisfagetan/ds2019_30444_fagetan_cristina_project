package ds.project.assignment14.dtos;

import ds.project.assignment14.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserForLoginDTO {


    private String username;
    private String password;


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static UserForLoginDTO generateDTOFromEntity(User user){
        return new UserForLoginDTO(
                user.getUsername(),
                user.getPassword());
    }

    public static User generateEntityFromDTO(UserForLoginDTO userForLoginDTO){
        return new User(
                userForLoginDTO.getUsername(),
                userForLoginDTO.getPassword());
    }

}
