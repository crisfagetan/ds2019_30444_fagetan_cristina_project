package ds.project.assignment14.validators;

import ds.project.assignment14.dtos.DrugDTO;
import ds.project.assignment14.errorhandlers.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class DrugFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(DrugFieldValidator.class);
//    private static final EmailFieldValidator EMAIL_VALIDATOR = new EmailFieldValidator() ;

    public static void validateInsertOrUpdate(DrugDTO drugDTO) {

        List<String> errors = new ArrayList<>();
        if (drugDTO == null) {
            errors.add("drugDTO is null");
            throw new IncorrectParameterException(DrugDTO.class.getSimpleName(), errors);
        }
//        if (drugDTO.getEmail() == null || !EMAIL_VALIDATOR.validate(drugDTO.getEmail())) {
//            errors.add("Person email has invalid format");
//        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DrugFieldValidator.class.getSimpleName(), errors);
        }
    }
}
