package ds.project.assignment4_doctor_producer.repositories;

import ds.project.assignment4_doctor_producer.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<UserEntity, Integer>{

    public UserEntity findUserByUsernameAndPassword(String username, String password);
}
