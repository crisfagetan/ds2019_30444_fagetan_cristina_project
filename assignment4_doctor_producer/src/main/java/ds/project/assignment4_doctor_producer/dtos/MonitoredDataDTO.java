package ds.project.assignment4_doctor_producer.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import doctorapp.doctor.MonitoredData;
import ds.project.assignment4_doctor_producer.entities.MonitoredDataEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoredDataDTO {

    int id;

    int patientId;

    Date startTime;

    Date endTime;

    String activity;

    String recommend;
    String status;

    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static MonitoredDataDTO generateDTOFromEntity(MonitoredDataEntity monitoredDataEntity){
        return new MonitoredDataDTO(
                monitoredDataEntity.getId(),
                monitoredDataEntity.getPatientId(),
                monitoredDataEntity.getStartTime(),
                monitoredDataEntity.getEndTime(),
                monitoredDataEntity.getActivity(),
                monitoredDataEntity.getRecommend(),
                monitoredDataEntity.getStatus());
    }

    public static MonitoredDataEntity generateEntityFromDTO(MonitoredDataDTO forViewDTO){
        return new MonitoredDataEntity(
                forViewDTO.patientId,
                forViewDTO.activity,
                forViewDTO.startTime,
                forViewDTO.endTime,
                forViewDTO.recommend,
                forViewDTO.status
        );
    }
}

