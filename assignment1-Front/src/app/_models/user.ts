export interface User {
  id: number;
  username: string;
  role: number;  // 0 = doctor; 1 = caregiver; 2 = patient
  userInRoleId: number;
}
