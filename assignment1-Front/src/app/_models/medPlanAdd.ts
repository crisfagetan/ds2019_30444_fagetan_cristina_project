import { Drug } from './drug';
import { Time } from '@angular/common';

export class MedPlanAdd {

  id: number;

  drugs?: Drug[];

  intakeCount?: number;

  intakeInterval?: number;

  startHour?: Time;

  startDate?: Date;

  endDate?: Date;
}
