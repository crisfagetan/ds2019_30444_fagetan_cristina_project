import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UserLogin } from '../_models/userLogin';
import { map } from 'rxjs/operators';
import { User } from '../_models/user';
import { UserRegister } from '../_models/userRegister';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  baseUrl = environment.apiUrl + 'user/';

constructor(private http: HttpClient) { }

login(model: any): Observable<User> {
  return this.http.post<User>(this.baseUrl + 'login', model);
}

register(model: any): Observable<User> {
  return this.http.post<User>(this.baseUrl + 'register', model);
}



}
