import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Patient } from '../_models/patient';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Activity } from '../_models/activity';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  baseUrl = environment.apiUrl + 'patient/';
  soapUrl = environment.soapUrl + '/doctor/activities/';

  constructor(private http: HttpClient) { }

    getPatients(): Observable<Patient[]> {
      return this.http.get<Patient[]>(this.baseUrl);
    }
    getPatient(id): Observable<Patient> {
      console.log('da');
      return this.http.get<Patient>(this.baseUrl + id);
    }

    updatePatient(patient: Patient) {
      return this.http.put(this.baseUrl, patient);
    }

    deletePatient(patient: Patient) {
      return this.http.put(this.baseUrl + '/delete', patient);
    }

    getActivities(id): Observable<Activity[]> {
      return this.http.get<Activity[]>(this.soapUrl + id );
    }

    getActivity(patientId, id): Observable<Activity> {
      return this.http.get<Activity>(this.soapUrl + patientId + '/' + id);
    }

    changeActivities(activity: Activity) {
      return this.http.put(this.soapUrl, activity );
    }
}
