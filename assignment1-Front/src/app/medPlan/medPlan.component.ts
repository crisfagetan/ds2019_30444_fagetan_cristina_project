import { Component, OnInit } from '@angular/core';
import { MedPlan } from '../_models/medPlan';
import { MedPlanService } from '../_services/medPlan.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-medPlan',
  templateUrl: './medPlan.component.html',
  styleUrls: ['./medPlan.component.css']
})
export class MedPlanComponent implements OnInit {
  medPlans: MedPlan[];
  isDoctor = false;
  public patientId: number;

  constructor(private medPlanService: MedPlanService, private alertify: AlertifyService,
              public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.showMedPlans();
    this.getPatientId();
    if (+localStorage.getItem('userRole') === 0) {
      this.isDoctor = true;
    } else {
      this.isDoctor = false;
    }
  }

  getPatientId() {
    this.patientId = +this.route.snapshot.paramMap.get('id');
    localStorage.setItem('patientId', this.patientId + '');
  }

  showMedPlans() {
    this.medPlanService.getMedPlans(+this.route.snapshot.paramMap.get('id')).subscribe((medPlans: MedPlan[]) => {
      this.medPlans = medPlans;
    }, error => {
      this.alertify.error(error);
    });
  }

//   deleteRow(drug) {
//     this.drugService.deleteDrug(drug).subscribe(next => {
//       this.alertify.success('Drug deleted successfully');
//       this.showDrugs();
//     }, error => {
//       this.alertify.error(error);
//     });
// }
}
