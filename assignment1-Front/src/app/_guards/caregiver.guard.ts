import { Injectable } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CaregiverGuard implements CanActivate {

  constructor(
    private router: Router,
    private alertify: AlertifyService
  ) {}

  canActivate(): boolean {
    if (
      localStorage.getItem('userRole') === '0' &&
      localStorage.getItem('out') === '0'
    ) {
      return true;
    }

    this.alertify.error('You shall not pass!!!');
    this.router.navigate(['/profile']);
    return false;
  }
}
