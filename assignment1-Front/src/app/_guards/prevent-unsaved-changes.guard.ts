import {Injectable} from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { DrugEditComponent } from '../drug-edit/drug-edit.component';

@Injectable()
export class PreventUnsavedChanges implements CanDeactivate<DrugEditComponent> {
    canDeactivate(component: DrugEditComponent) {
        // if (component.editForm.dirty) {
        //     return confirm('Are you sure you want to continue?  Any unsaved changes will be lost');
        // }
        return true;
    }
}
